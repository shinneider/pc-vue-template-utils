var webpack = require("webpack");
const path = require("path");
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
  entry: {
    WppBtn: "./src/buttons/WppBtn.vue",
    FloatContainer: "./src/floatings/FloatContainer.vue",
  },
  output: {
    path: path.resolve(__dirname + "/dist/"),
    publicPath: "dist/",
    filename: "[name].js",
    libraryTarget: "umd",
    library: "[name]",
    umdNamedDefine: true,
  },
  mode: "production",
  optimization: {
    minimize: true,
  },
  resolve: {
    extensions: [".js"],
    alias: {
      vue$: "vue/dist/vue.common.js",
    },
  },
  devtool: "#source-map",
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, "src"),
          path.resolve(__dirname, "node_module/pc-vue-icons"),
        ],
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.vue$/,
        use: "vue-loader",
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)(\?.*$|$)/,
        loader: "file-loader",
      },
    ],
  },
  externals: {
    vue: "Vue",
  },
  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1,
    }),
    new VueLoaderPlugin(),
  ],
};
